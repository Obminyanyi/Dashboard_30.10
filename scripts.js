var closeLeftMenu = document.getElementById('CloseLeftMenu');
var leftMenu = document.getElementById('LeftMenu');
var showRightMenu = document.getElementById('ShowRightMenu');
var rightMenu = document.getElementById('RightMenu');

showRightMenu.onclick = function(event) {
    rightMenu.classList.toggle('active');
}
closeLeftMenu.onclick = function(event) {
    leftMenu.classList.toggle('active');
};;

(function() {
    var ProjectView = function() {
        //2
        this.template = '<div class="cell-div-name">%(name)</div>' +
            ' <div class="cell-div">%(data)</div>' +
            ' <div class="cell-div">%(creater)</div>' +
            '<div class="cell-div">%(members)</div>' +
            '<div class="cell-div">%(type)</div>' +
            ' <div class="cell-div">%(status)</div>' +
            '<div class="cell-div">%(customers)</div>' +
            '<div class="trash"></div>';
        this.init();

    };
    ProjectView.prototype.renderProject = function(item) {
        //5
        var projectBlock = this.template.replace(/%\((.+?)\)/g, function(expr, paramName) {
            if (paramName in item) {
                return item[paramName];
            }
            return expr;
        });

        var row = document.createElement("div");
        row.classList.add("row");
        row.innerHTML = projectBlock;
        return row;
    }
    ProjectView.prototype.renderAllProject = function(obj) {
        //4
        var allItems = document.createDocumentFragment();
        var rowTemplate;
        for (project in obj) {
            rowTemplate = this.renderProject(obj[project]);
            allItems.appendChild(rowTemplate)
        }

        return allItems;
    }
    ProjectView.prototype.init = function() {
        //3
        var json = JSON.parse(window.myJSON)
        // json["dasdasdasd"] = {
        //     name: "project name5",
        //      data: "data",
        // creater: "creater",
        // members: ["members"],
        // type: "type",
        // status: "status",
        // customers: [1, 1, 1]
        // }
        var allItems = this.renderAllProject(json);
        var putPlace = document.getElementsByClassName('table')[0];
        putPlace.appendChild(allItems);
    }
    //1
    var a = new ProjectView;
})();

// $(".button").click(function() {
//     $('.toggled_block').toggle();
// });
// $(document).on('click', function(e) {
//     if ($(e.target).hasClass('menu-toggle')) {
//         return true;
//     }

//     if (!$(e.target).closest(".right-menu").length) {
//         $('.right-menu').toggleClass('active')
//     }

//     e.stopPropagation();
// });

// ________________SEARCH______________________________________



var input, search, pr, result, result_arr, locale_HTML, result_store;

locale_HTML = document.body.innerHTML;

function FindOnPage(name, status) {

    input = document.getElementById(name).value;

    if (input.length < 3 && status == true) {
        alert('Введите не менее трех символов!');

        function FindOnPageBack() { document.body.innerHTML = locale_HTML; }
    }

    if (input.length >= 3) {
        function FindOnPageGo() {
            search = '/' + input + '/g';
            pr = document.body.innerHTML;
            result = pr.match(/>(.*?)</g);
            result_arr = [];

            for (var i = 0; i < result.length; i++) {
                result_arr[i] = result[i].replace(eval(search), '<span style="background-color:lightgrey;">' + input + '</span>');
            }
            for (var i = 0; i < result.length; i++) {
                pr = pr.replace(result[i], result_arr[i])
            }
            document.body.innerHTML = pr;
        }
    }

    function FindOnPageBack() { document.body.innerHTML = locale_HTML; }
    if (status) { FindOnPageBack();
        FindOnPageGo(); }
    if (!status) { FindOnPageBack(); }
    var warning = true;
    for (var i = 0; i < result.length; i++) {
        if (result[i].match(eval(search)) != null) {
            warning = false;
        }
    }
    if (warning == true) {
        alert('Совпадений не найдено!');
    }
}